package com.datascouting.dachs.extension.security;

import com.datascouting.dachs.extension.domain.DachsExtensionDomainConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@Import(DachsExtensionDomainConfiguration.class)
@EnableGlobalMethodSecurity(prePostEnabled = true)
@ComponentScan
@Configuration
public class DachsExtensionSecurityConfiguration {

    @Bean
    RoleHierarchy roleHierarchy() {
        final RoleHierarchyImpl roleHierarchy = new RoleHierarchyImpl();

        roleHierarchy.setHierarchy("ROLE_ADMIN > ROLE_CURATOR");

        return roleHierarchy;
    }
}
