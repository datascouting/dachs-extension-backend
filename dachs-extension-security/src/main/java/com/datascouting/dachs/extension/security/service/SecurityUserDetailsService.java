package com.datascouting.dachs.extension.security.service;

import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.domain.repository.UserRepository;
import com.datascouting.dachs.extension.security.model.SecurityUser;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.function.Function;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class SecurityUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public SecurityUser loadUserByUsername(final String username) throws UsernameNotFoundException {
        final Function<User, SecurityUser> userToCustomUserDetails = user -> {
            final SecurityUser userDetails = new SecurityUser();

            BeanUtils.copyProperties(user, userDetails);

            return userDetails;
        };

        return Try.of(() -> userRepository.findByEmail(username))
                .flatMap(userOption -> userOption.toTry(() -> new UsernameNotFoundException(username)))
                .map(userToCustomUserDetails)
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);
    }
}
