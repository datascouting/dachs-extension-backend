package com.datascouting.dachs.extension.security.model;

/**
 * @author Achilleas Naoumidis
 */
public abstract class Roles {

    public static String ADMIN = "ADMIN";

    public static String CURATOR = "CURATOR";
}
