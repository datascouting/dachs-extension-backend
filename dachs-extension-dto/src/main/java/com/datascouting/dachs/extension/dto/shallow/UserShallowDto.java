package com.datascouting.dachs.extension.dto.shallow;

import com.datascouting.dachs.extension.dto.AbstractDto;
import com.datascouting.dachs.extension.dto.ReferenceDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserShallowDto extends AbstractDto {

    private String email;

    private String password;

    private Set<ReferenceDto> roles;
}
