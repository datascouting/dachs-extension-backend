package com.datascouting.dachs.extension.dto.deep;

import com.datascouting.dachs.extension.dto.AbstractDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AnnotationDeepDto extends AbstractDto {

    private DocumentDeepDto document;

    private ClassificationDeepDto classification;

    private String notes;
}
