package com.datascouting.dachs.extension.dto.deep;

import com.datascouting.dachs.extension.dto.AbstractDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Set;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class UserDeepDto extends AbstractDto {

    private String email;

    private String password;

    private Set<RoleDeepDto> roles;
}
