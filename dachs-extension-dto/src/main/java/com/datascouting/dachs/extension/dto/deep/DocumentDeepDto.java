package com.datascouting.dachs.extension.dto.deep;

import com.datascouting.dachs.extension.dto.AbstractDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DocumentDeepDto extends AbstractDto {

    private SourceDeepDto source;

    private String referenceId;

    private String text;
}
