package com.datascouting.dachs.extension.dto.deep;

import com.datascouting.dachs.extension.dto.AbstractDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SourceDeepDto extends AbstractDto {

    private String name;

    private String domain;

    private String description;
}
