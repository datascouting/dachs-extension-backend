package com.datascouting.dachs.extension.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Achilleas Naoumidis
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ReferenceDto extends AbstractDto {

    public ReferenceDto() {
    }

    public ReferenceDto(String id) {
        this.id = id;
    }
}
