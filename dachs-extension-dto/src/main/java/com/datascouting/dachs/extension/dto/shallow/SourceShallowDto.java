package com.datascouting.dachs.extension.dto.shallow;

import com.datascouting.dachs.extension.dto.AbstractDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class SourceShallowDto extends AbstractDto {

    private String name;

    private String domain;

    private String description;
}
