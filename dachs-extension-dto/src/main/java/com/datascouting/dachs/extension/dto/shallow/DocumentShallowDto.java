package com.datascouting.dachs.extension.dto.shallow;

import com.datascouting.dachs.extension.dto.AbstractDto;
import com.datascouting.dachs.extension.dto.ReferenceDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class DocumentShallowDto extends AbstractDto {

    private ReferenceDto source;

    private String referenceId;

    private String text;
}
