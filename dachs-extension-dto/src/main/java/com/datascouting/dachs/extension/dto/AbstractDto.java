package com.datascouting.dachs.extension.dto;

import lombok.Data;
import org.intellift.sol.domain.Identifiable;

import java.util.Date;

/**
 * @author Achilleas Naoumidis
 */
@Data
public abstract class AbstractDto implements Identifiable<String> {

    protected String id;

    protected Date createdAt;

    protected Date updatedAt;

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "id='" + id + '\'' +
                '}';
    }
}
