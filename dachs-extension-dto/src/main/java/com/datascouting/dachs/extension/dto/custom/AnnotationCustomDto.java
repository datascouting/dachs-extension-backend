package com.datascouting.dachs.extension.dto.custom;

import com.datascouting.dachs.extension.dto.AbstractDto;
import com.datascouting.dachs.extension.dto.ReferenceDto;
import com.datascouting.dachs.extension.dto.shallow.DocumentShallowDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class AnnotationCustomDto extends AbstractDto {

    private DocumentShallowDto document;

    private ReferenceDto classification;

    private String notes;
}
