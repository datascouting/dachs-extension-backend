package com.datascouting.dachs.extension.service.mail.template;

import com.datascouting.dachs.extension.domain.entity.User;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.Locale;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class TemplateServiceImpl implements TemplateService {

    private final TemplateEngine templateEngine;

    @Override
    public Try<String> buildRegisterSuccessTemplate(final User user, final Locale locale) {
        requireNonNull(user, "user is null");
        requireNonNull(locale, "locale is null");

        return Try.of(() -> new Context(locale))
                .map(context -> templateEngine.process(
                        "mail/register-success",
                        context
                ));
    }
}
