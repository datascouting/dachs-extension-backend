package com.datascouting.dachs.extension.service.classification;

import com.datascouting.dachs.extension.domain.entity.Classification;
import com.datascouting.dachs.extension.service.AbstractCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface ClassificationCrudService extends AbstractCrudService<Classification> {
}
