package com.datascouting.dachs.extension.service.mail.action;

import com.datascouting.dachs.extension.domain.entity.User;
import javaslang.control.Try;

import java.util.Locale;

public interface RegisterMailAction {

    Try<Void> sendSuccessfullyRegisterMail(User user, Locale locale);
}
