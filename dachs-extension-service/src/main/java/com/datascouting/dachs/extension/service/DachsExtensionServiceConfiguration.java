package com.datascouting.dachs.extension.service;

import com.datascouting.dachs.extension.security.DachsExtensionSecurityConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(DachsExtensionSecurityConfiguration.class)
@ComponentScan
@Configuration
public class DachsExtensionServiceConfiguration {
}
