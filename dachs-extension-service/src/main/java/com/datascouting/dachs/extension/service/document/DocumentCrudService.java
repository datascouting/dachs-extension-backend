package com.datascouting.dachs.extension.service.document;

import com.datascouting.dachs.extension.domain.entity.Document;
import com.datascouting.dachs.extension.service.AbstractCrudService;
import javaslang.control.Option;
import javaslang.control.Try;

import java.util.function.Supplier;

/**
 * @author Achilleas Naoumidis
 */
public interface DocumentCrudService extends AbstractCrudService<Document> {

    Try<Option<Document>> findOneByReferenceId(String referenceId);

    Try<Document> findOneByReferenceId(String referenceId, Supplier<? extends Exception> ifNotFound);
}
