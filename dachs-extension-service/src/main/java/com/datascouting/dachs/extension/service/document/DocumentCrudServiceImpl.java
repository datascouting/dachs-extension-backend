package com.datascouting.dachs.extension.service.document;

import com.datascouting.dachs.extension.domain.entity.Document;
import com.datascouting.dachs.extension.domain.repository.DocumentRepository;
import javaslang.control.Option;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.function.Supplier;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class DocumentCrudServiceImpl implements DocumentCrudService {

    private final DocumentRepository documentRepository;

    @Override
    public DocumentRepository getRepository() {
        return documentRepository;
    }

    @Override
    public Try<Option<Document>> findOneByReferenceId(@Nonnull String referenceId) {
        return Try.of(() -> getRepository().findOneByReferenceId(referenceId));
    }

    @Override
    public Try<Document> findOneByReferenceId(@Nonnull String referenceId,
                                              @Nonnull Supplier<? extends Exception> ifNotFound) {
        return findOneByReferenceId(referenceId)
                .flatMap(documentOption -> documentOption.toTry(ifNotFound));
    }
}
