package com.datascouting.dachs.extension.service.role;

import com.datascouting.dachs.extension.domain.entity.Role;
import com.datascouting.dachs.extension.service.AbstractCrudService;
import javaslang.control.Try;

import java.util.Set;

/**
 * @author Achilleas Naoumidis
 */
public interface RoleCrudService extends AbstractCrudService<Role> {

    Try<Set<Role>> getCuratorRoles();
}
