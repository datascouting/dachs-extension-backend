package com.datascouting.dachs.extension.service.classification;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class ClassificationServiceImpl implements ClassificationService {

    private final ClassificationCrudService classificationCrudService;

    private final ClassificationUtilities classificationUtilities;
}
