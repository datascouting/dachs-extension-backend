package com.datascouting.dachs.extension.service.document;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {

    private final DocumentCrudService documentCrudService;

    private final DocumentUtilities documentUtilities;
}
