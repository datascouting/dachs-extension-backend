package com.datascouting.dachs.extension.service.classification;

import com.datascouting.dachs.extension.domain.repository.ClassificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class ClassificationCrudServiceImpl implements ClassificationCrudService {

    private final ClassificationRepository classificationRepository;

    private final ClassificationUtilities classificationUtilities;

    @Override
    public ClassificationRepository getRepository() {
        return classificationRepository;
    }
}
