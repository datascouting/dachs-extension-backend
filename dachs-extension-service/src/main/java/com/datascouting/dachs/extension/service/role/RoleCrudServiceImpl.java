package com.datascouting.dachs.extension.service.role;

import com.datascouting.dachs.extension.domain.entity.QRole;
import com.datascouting.dachs.extension.domain.entity.Role;
import com.datascouting.dachs.extension.domain.repository.RoleRepository;
import com.datascouting.dachs.extension.security.model.Roles;
import javaslang.Value;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class RoleCrudServiceImpl implements RoleCrudService {

    private final RoleRepository roleRepository;

    @Override
    public RoleRepository getRepository() {
        return roleRepository;
    }

    @Override
    public Try<Set<Role>> getCuratorRoles() {
        return findAll(QRole.role.name.eq("ROLE_" + Roles.CURATOR))
                .map(Value::toJavaSet);
    }
}
