package com.datascouting.dachs.extension.service.user;

import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.service.AbstractCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface UserCrudService extends AbstractCrudService<User> {
}
