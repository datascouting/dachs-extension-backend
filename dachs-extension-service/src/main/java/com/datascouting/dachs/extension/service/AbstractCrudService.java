package com.datascouting.dachs.extension.service;

import com.datascouting.dachs.extension.domain.entity.AbstractEntity;
import org.intellift.sol.service.querydsl.QueryDslCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface AbstractCrudService<E extends AbstractEntity> extends QueryDslCrudService<E, String> {
}
