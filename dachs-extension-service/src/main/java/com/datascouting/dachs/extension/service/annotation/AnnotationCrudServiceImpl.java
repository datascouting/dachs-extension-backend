package com.datascouting.dachs.extension.service.annotation;

import com.datascouting.dachs.extension.domain.repository.AnnotationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class AnnotationCrudServiceImpl implements AnnotationCrudService {

    private final AnnotationRepository annotationRepository;

    private final AnnotationUtilities annotationUtilities;

    @Override
    public AnnotationRepository getRepository() {
        return annotationRepository;
    }
}
