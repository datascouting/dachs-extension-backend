package com.datascouting.dachs.extension.service.annotation;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import com.datascouting.dachs.extension.service.AbstractCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface AnnotationCrudService extends AbstractCrudService<Annotation> {
}
