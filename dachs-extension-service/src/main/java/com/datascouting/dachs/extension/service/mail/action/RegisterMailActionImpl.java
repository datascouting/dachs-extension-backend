package com.datascouting.dachs.extension.service.mail.action;

import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.service.mail.model.Mail;
import com.datascouting.dachs.extension.service.mail.sender.MailSender;
import com.datascouting.dachs.extension.service.mail.template.TemplateService;
import com.google.common.collect.Sets;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Locale;

import static java.util.Objects.requireNonNull;

@Service
@RequiredArgsConstructor
public class RegisterMailActionImpl implements RegisterMailAction {

    private final Environment environment;
    private final MailSender mailSender;
    private final TemplateService templateService;
    private final MessageSource messageSource;

    @Override
    public Try<Void> sendSuccessfullyRegisterMail(final User user, final Locale locale) {
        requireNonNull(user, "user is null");
        requireNonNull(locale, "locale is null");

        return templateService.buildRegisterSuccessTemplate(user, locale)
                .map(html -> Mail.builder()
                        .to(Sets.newHashSet(user.getEmail()))
                        .from(environment.getProperty("spring.mail.username"))
                        .subject(messageSource.getMessage("register-mail-subject", null, locale))
                        .body(html)
                        .build())
                .flatMap(mailSender::send);
    }
}
