package com.datascouting.dachs.extension.service.role;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleCrudService roleCrudService;

    private final RoleUtilities roleUtilities;
}
