package com.datascouting.dachs.extension.service.source;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class SourceServiceImpl implements SourceService {

    private final SourceCrudService sourceCrudService;

    private final SourceUtilities sourceUtilities;
}
