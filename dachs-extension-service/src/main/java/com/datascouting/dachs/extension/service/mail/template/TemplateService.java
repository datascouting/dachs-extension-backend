package com.datascouting.dachs.extension.service.mail.template;

import com.datascouting.dachs.extension.domain.entity.User;
import javaslang.control.Try;

import java.util.Locale;

public interface TemplateService {

    Try<String> buildRegisterSuccessTemplate(User user, Locale locale);
}
