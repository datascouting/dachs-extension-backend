package com.datascouting.dachs.extension.service.annotation;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import com.datascouting.dachs.extension.domain.entity.Classification;
import com.datascouting.dachs.extension.domain.entity.Document;
import com.datascouting.dachs.extension.service.document.DocumentCrudService;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;

import static javaslang.API.*;
import static javaslang.Patterns.None;
import static javaslang.Patterns.Some;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class AnnotationServiceImpl implements AnnotationService {

    private final AnnotationCrudService annotationCrudService;

    private final AnnotationUtilities annotationUtilities;

    private final DocumentCrudService documentCrudService;

    @Override
    public Try<Annotation> annotate(@Nonnull Annotation annotation) {
        final Document document = annotation.getDocument();
        final Classification classification = annotation.getClassification();
        final String notes = annotation.getNotes();

        return documentCrudService.findOneByReferenceId(document.getReferenceId())
                .flatMap(documentOption -> Match(documentOption).of(

                        Case(Some($()), Try::success),

                        Case(None(), () -> documentCrudService.create(document))
                ))
                .map(persistedDocument -> {
                    final Annotation validAnnotation = new Annotation();

                    validAnnotation.setDocument(persistedDocument);
                    validAnnotation.setClassification(classification);
                    validAnnotation.setNotes(notes);

                    return validAnnotation;
                })
                .flatMap(annotationCrudService::create);
    }
}
