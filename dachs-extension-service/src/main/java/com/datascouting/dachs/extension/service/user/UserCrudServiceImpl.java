package com.datascouting.dachs.extension.service.user;

import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.domain.repository.UserRepository;
import com.datascouting.dachs.extension.service.mail.action.RegisterMailAction;
import com.datascouting.dachs.extension.service.role.RoleCrudService;
import javaslang.collection.List;
import javaslang.collection.Traversable;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import static java.util.Objects.requireNonNull;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class UserCrudServiceImpl implements UserCrudService {

    private final UserRepository userRepository;
    private final RoleCrudService roleCrudService;
    private final RegisterMailAction registerMailAction;

    @Override
    public UserRepository getRepository() {
        return userRepository;
    }

    @Override
    public Try<User> create(final User entity) {
        requireNonNull(entity, "entity is null");

        return roleCrudService.getCuratorRoles()
                .map(roles -> {
                    entity.setRoles(roles);
                    return entity;
                })
                .flatMap(user -> save(user)
                        .flatMap(savedUser -> registerMailAction.sendSuccessfullyRegisterMail(
                                savedUser,
                                LocaleContextHolder.getLocale()
                        ).map(aVoid -> savedUser)));
    }

    @Override
    public Try<List<User>> create(final Traversable<User> entities) {
        requireNonNull(entities, "entities is null");

        return roleCrudService.getCuratorRoles()
                .map(roles -> {
                    for (User user : entities) {
                        user.setRoles(roles);
                    }
                    return entities;
                })
                .flatMap(this::save);
    }
}
