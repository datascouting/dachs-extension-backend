package com.datascouting.dachs.extension.service.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserCrudService userCrudService;

    private final UserUtilities userUtilities;
}
