package com.datascouting.dachs.extension.service.source;

import com.datascouting.dachs.extension.domain.entity.Source;
import com.datascouting.dachs.extension.service.AbstractCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface SourceCrudService extends AbstractCrudService<Source> {
}
