package com.datascouting.dachs.extension.service.annotation;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import javaslang.control.Try;

/**
 * @author Achilleas Naoumidis
 */
public interface AnnotationService {

    Try<Annotation> annotate(Annotation annotation);
}
