package com.datascouting.dachs.extension.service.source;

import com.datascouting.dachs.extension.domain.repository.SourceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Achilleas Naoumidis
 */
@Service
@RequiredArgsConstructor
public class SourceCrudServiceImpl implements SourceCrudService {

    private final SourceRepository sourceRepository;

    @Override
    public SourceRepository getRepository() {
        return sourceRepository;
    }
}
