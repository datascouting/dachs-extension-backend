package com.datascouting.dachs.extension.service.mail.sender;

import com.datascouting.dachs.extension.service.mail.model.Mail;
import javaslang.control.Try;

public interface MailSender {

    Try<Void> send(final Mail mail);
}
