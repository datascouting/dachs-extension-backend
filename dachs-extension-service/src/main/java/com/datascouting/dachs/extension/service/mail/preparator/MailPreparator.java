package com.datascouting.dachs.extension.service.mail.preparator;

import com.datascouting.dachs.extension.service.mail.model.Mail;
import javaslang.control.Option;
import javaslang.control.Try;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import java.util.Date;
import java.util.Objects;

public class MailPreparator {

    public static MimeMessagePreparator prepare(final Mail mail) {
        Objects.requireNonNull(mail, "mail is null");

        return mimeMessage -> {
            final MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true);
            message.setFrom(Option.of(mail.getFrom()).getOrElse(""));
            message.setText(Option.of(mail.getBody()).getOrElse(""), true);
            message.setSubject(Option.of(mail.getSubject()).getOrElse(""));
            message.setSentDate(new Date());

            Option.of(mail.getTo())
                    .map(to -> to.toArray(new String[0]))
                    .forEach(strings -> Try.run(() -> message.setTo(strings)));

            Option.of(mail.getBcc())
                    .map(bcc -> bcc.toArray(new String[0]))
                    .forEach(strings -> Try.run(() -> message.setBcc(strings)));

            Option.of(mail.getCc())
                    .map(cc -> cc.toArray(new String[0]))
                    .forEach(strings -> Try.run(() -> message.setCc(strings)));

            // TODO add attachments
        };
    }
}
