package com.datascouting.dachs.extension.mapper.deep;

import com.datascouting.dachs.extension.domain.entity.Document;
import com.datascouting.dachs.extension.dto.deep.DocumentDeepDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper(uses = SourceDeepMapper.class)
public interface DocumentDeepMapper extends AbstractMapper<Document, DocumentDeepDto> {
}
