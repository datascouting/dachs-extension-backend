package com.datascouting.dachs.extension.mapper.deep;

import com.datascouting.dachs.extension.domain.entity.Role;
import com.datascouting.dachs.extension.dto.deep.RoleDeepDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper
public interface RoleDeepMapper extends AbstractMapper<Role, RoleDeepDto> {
}
