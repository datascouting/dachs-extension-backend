package com.datascouting.dachs.extension.mapper.custom;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import com.datascouting.dachs.extension.dto.custom.AnnotationCustomDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import com.datascouting.dachs.extension.mapper.ReferenceMapper;
import com.datascouting.dachs.extension.mapper.shallow.DocumentShallowMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper(uses = {
        DocumentShallowMapper.class,
        ReferenceMapper.class
})
public interface AnnotationCustomMapper extends AbstractMapper<Annotation, AnnotationCustomDto> {
}
