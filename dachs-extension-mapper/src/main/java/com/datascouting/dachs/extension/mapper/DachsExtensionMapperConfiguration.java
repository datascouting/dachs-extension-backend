package com.datascouting.dachs.extension.mapper;

import com.datascouting.dachs.extension.domain.DachsExtensionDomainConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(DachsExtensionDomainConfiguration.class)
@ComponentScan
@Configuration
public class DachsExtensionMapperConfiguration {
}
