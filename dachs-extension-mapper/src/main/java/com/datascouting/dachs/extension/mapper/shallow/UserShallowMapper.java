package com.datascouting.dachs.extension.mapper.shallow;

import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.dto.shallow.UserShallowDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import com.datascouting.dachs.extension.mapper.ReferenceMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper(uses = ReferenceMapper.class)
public interface UserShallowMapper extends AbstractMapper<User, UserShallowDto> {
}
