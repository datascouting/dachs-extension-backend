package com.datascouting.dachs.extension.mapper.deep;

import com.datascouting.dachs.extension.domain.entity.Classification;
import com.datascouting.dachs.extension.dto.deep.ClassificationDeepDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper
public interface ClassificationDeepMapper extends AbstractMapper<Classification, ClassificationDeepDto> {
}
