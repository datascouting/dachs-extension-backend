package com.datascouting.dachs.extension.mapper.deep;

import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.dto.deep.UserDeepDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper(uses = RoleDeepMapper.class)
public interface UserDeepMapper extends AbstractMapper<User, UserDeepDto> {
}
