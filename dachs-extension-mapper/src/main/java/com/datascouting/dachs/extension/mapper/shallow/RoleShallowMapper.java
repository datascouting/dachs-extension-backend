package com.datascouting.dachs.extension.mapper.shallow;

import com.datascouting.dachs.extension.domain.entity.Role;
import com.datascouting.dachs.extension.dto.shallow.RoleShallowDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper
public interface RoleShallowMapper extends AbstractMapper<Role, RoleShallowDto> {
}
