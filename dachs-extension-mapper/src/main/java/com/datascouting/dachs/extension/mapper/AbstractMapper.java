package com.datascouting.dachs.extension.mapper;

import com.datascouting.dachs.extension.domain.entity.AbstractEntity;
import com.datascouting.dachs.extension.dto.AbstractDto;
import org.intellift.sol.mapper.PageMapper;

/**
 * @author Achilleas Naoumidis
 */
public interface AbstractMapper<E extends AbstractEntity, D extends AbstractDto> extends PageMapper<E, D> {
}
