package com.datascouting.dachs.extension.mapper.shallow;

import com.datascouting.dachs.extension.domain.entity.Classification;
import com.datascouting.dachs.extension.dto.shallow.ClassificationShallowDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper
public interface ClassificationShallowMapper extends AbstractMapper<Classification, ClassificationShallowDto> {
}
