package com.datascouting.dachs.extension.mapper.deep;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import com.datascouting.dachs.extension.dto.deep.AnnotationDeepDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper(uses = {
        DocumentDeepMapper.class,
        ClassificationDeepMapper.class
})
public interface AnnotationDeepMapper extends AbstractMapper<Annotation, AnnotationDeepDto> {
}
