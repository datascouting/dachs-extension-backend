package com.datascouting.dachs.extension.mapper.shallow;

import com.datascouting.dachs.extension.domain.entity.Source;
import com.datascouting.dachs.extension.dto.shallow.SourceShallowDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper
public interface SourceShallowMapper extends AbstractMapper<Source, SourceShallowDto> {
}
