package com.datascouting.dachs.extension.mapper;

import com.datascouting.dachs.extension.domain.entity.AbstractEntity;
import com.datascouting.dachs.extension.dto.ReferenceDto;
import lombok.RequiredArgsConstructor;
import org.mapstruct.TargetType;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class ReferenceMapper {

    private final EntityManager entityManager;

    public <E extends AbstractEntity> E resolve(final ReferenceDto reference, @TargetType final Class<E> entityClass) {
        return Objects.nonNull(reference)
                ? entityManager.find(entityClass, reference.getId())
                : null;
    }

    public ReferenceDto toReference(final AbstractEntity entity) {
        return Objects.nonNull(entity)
                ? new ReferenceDto(entity.getId())
                : null;
    }
}
