package com.datascouting.dachs.extension.mapper.shallow;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import com.datascouting.dachs.extension.dto.shallow.AnnotationShallowDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import com.datascouting.dachs.extension.mapper.ReferenceMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper(uses = ReferenceMapper.class)
public interface AnnotationShallowMapper extends AbstractMapper<Annotation, AnnotationShallowDto> {
}
