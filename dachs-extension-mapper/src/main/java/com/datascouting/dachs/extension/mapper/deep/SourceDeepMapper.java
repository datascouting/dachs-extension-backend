package com.datascouting.dachs.extension.mapper.deep;

import com.datascouting.dachs.extension.domain.entity.Source;
import com.datascouting.dachs.extension.dto.deep.SourceDeepDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import org.mapstruct.Mapper;

/**
 * @author Achilleas Naoumidis
 */
@Mapper
public interface SourceDeepMapper extends AbstractMapper<Source, SourceDeepDto> {
}
