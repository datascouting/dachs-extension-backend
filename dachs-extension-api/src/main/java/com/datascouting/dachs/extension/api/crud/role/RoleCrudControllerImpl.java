package com.datascouting.dachs.extension.api.crud.role;

import com.datascouting.dachs.extension.domain.entity.Role;
import com.datascouting.dachs.extension.dto.deep.RoleDeepDto;
import com.datascouting.dachs.extension.mapper.deep.RoleDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.RoleShallowMapper;
import com.datascouting.dachs.extension.service.role.RoleCrudService;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Achilleas Naoumidis
 */
@RequestMapping("/roles")
@RestController
@RequiredArgsConstructor
public class RoleCrudControllerImpl implements RoleCrudController {

    private final RoleDeepMapper roleDeepMapper;

    private final RoleShallowMapper roleShallowMapper;

    private final RoleCrudService roleCrudService;

    @Override
    public RoleDeepMapper getMapper() {
        return roleDeepMapper;
    }

    @Override
    public RoleShallowMapper getReferenceMapper() {
        return roleShallowMapper;
    }

    @Override
    public RoleCrudService getService() {
        return roleCrudService;
    }

    @Override
    public ResponseEntity<Page<RoleDeepDto>> getAll(@QuerydslPredicate(root = Role.class) Predicate predicate, Pageable pageable) {
        return getAllDefaultImplementation(predicate, pageable);
    }
}
