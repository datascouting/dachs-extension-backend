package com.datascouting.dachs.extension.api.configuration.security;

import com.datascouting.dachs.extension.api.configuration.security.handler.CustomAuthenticationFailureHandler;
import com.datascouting.dachs.extension.api.configuration.security.handler.CustomAuthenticationSuccessHandler;
import com.datascouting.dachs.extension.api.configuration.security.handler.CustomLogoutSuccessHandler;
import com.datascouting.dachs.extension.security.service.SecurityUserDetailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

import static com.datascouting.dachs.extension.security.model.Roles.ADMIN;
import static com.datascouting.dachs.extension.security.model.Roles.CURATOR;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

/**
 * @author Achilleas Naoumidis
 */
@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final SecurityUserDetailsService securityUserDetailsService;

    private final PasswordEncoder passwordEncoder;

    private final CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;

    private final CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

    private final CustomLogoutSuccessHandler customLogoutSuccessHandler;

    @Override
    public void configure(final AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(securityUserDetailsService)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(final HttpSecurity security) throws Exception {
        security
                .authorizeRequests()

                .mvcMatchers(publicPatterns())
                .permitAll()

                .mvcMatchers(GET, curationGetPatterns())
                .hasRole(CURATOR)

                .mvcMatchers(POST, curationPostPatterns())
                .hasRole(CURATOR)

                .anyRequest()
                .hasRole(ADMIN)

                .and()

                .csrf()
                .disable()

                .formLogin()
                .permitAll()
                .successHandler(customAuthenticationSuccessHandler)
                .failureHandler(customAuthenticationFailureHandler)

                .and()

                .logout()
                .logoutSuccessHandler(customLogoutSuccessHandler)

                .and()

                .rememberMe()

                .and()

                .cors()

                .and()

                .exceptionHandling()
                .authenticationEntryPoint(new HttpStatusEntryPoint(UNAUTHORIZED));
    }

    private String[] publicPatterns() {
        return new String[]{
                "/favicon.ico",
                "/robots.txt",
                "/register",
                "/register-success",

                "/css/**",
                "/vendor/**",
                "/img/**"
        };
    }

    private String[] curationGetPatterns() {
        return new String[]{
                "/sources/**",
                "/classifications/**"
        };
    }

    private String[] curationPostPatterns() {
        return new String[]{
                "/documents",
                "/annotations",
                "/rpc/annotation"
        };
    }
}
