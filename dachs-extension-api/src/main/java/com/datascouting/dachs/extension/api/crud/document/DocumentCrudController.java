package com.datascouting.dachs.extension.api.crud.document;

import com.datascouting.dachs.extension.api.crud.AbstractCrudController;
import com.datascouting.dachs.extension.domain.entity.Document;
import com.datascouting.dachs.extension.dto.deep.DocumentDeepDto;
import com.datascouting.dachs.extension.dto.shallow.DocumentShallowDto;
import com.datascouting.dachs.extension.mapper.deep.DocumentDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.DocumentShallowMapper;
import com.datascouting.dachs.extension.service.document.DocumentCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface DocumentCrudController extends AbstractCrudController<Document, DocumentDeepDto, DocumentShallowDto> {

    @Override
    DocumentDeepMapper getMapper();

    @Override
    DocumentShallowMapper getReferenceMapper();

    @Override
    DocumentCrudService getService();
}
