package com.datascouting.dachs.extension.api.crud;

import com.datascouting.dachs.extension.domain.entity.AbstractEntity;
import com.datascouting.dachs.extension.dto.AbstractDto;
import com.datascouting.dachs.extension.mapper.AbstractMapper;
import com.datascouting.dachs.extension.service.AbstractCrudService;
import org.intellift.sol.controller.querydsl.api.QueryDslAsymmetricCrudApiController;

public interface AbstractCrudController<E extends AbstractEntity, DD extends AbstractDto, SD extends AbstractDto> extends QueryDslAsymmetricCrudApiController<E, DD, SD, String> {

    @Override
    AbstractMapper<E, DD> getMapper();

    @Override
    AbstractMapper<E, SD> getReferenceMapper();

    @Override
    AbstractCrudService<E> getService();
}
