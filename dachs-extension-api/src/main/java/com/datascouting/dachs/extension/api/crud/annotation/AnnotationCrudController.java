package com.datascouting.dachs.extension.api.crud.annotation;

import com.datascouting.dachs.extension.api.crud.AbstractCrudController;
import com.datascouting.dachs.extension.domain.entity.Annotation;
import com.datascouting.dachs.extension.dto.deep.AnnotationDeepDto;
import com.datascouting.dachs.extension.dto.shallow.AnnotationShallowDto;
import com.datascouting.dachs.extension.mapper.deep.AnnotationDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.AnnotationShallowMapper;
import com.datascouting.dachs.extension.service.annotation.AnnotationCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface AnnotationCrudController extends AbstractCrudController<Annotation, AnnotationDeepDto, AnnotationShallowDto> {

    @Override
    AnnotationDeepMapper getMapper();

    @Override
    AnnotationShallowMapper getReferenceMapper();

    @Override
    AnnotationCrudService getService();
}
