package com.datascouting.dachs.extension.api.rpc.annotation.annotation;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import com.datascouting.dachs.extension.dto.custom.AnnotationCustomDto;
import com.datascouting.dachs.extension.dto.deep.AnnotationDeepDto;
import com.datascouting.dachs.extension.mapper.custom.AnnotationCustomMapper;
import com.datascouting.dachs.extension.mapper.deep.AnnotationDeepMapper;
import com.datascouting.dachs.extension.service.annotation.AnnotationService;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * @author Achilleas Naoumidis
 */
@RequestMapping("/rpc/annotation")
@RestController
@RequiredArgsConstructor
public class AnnotationRpcControllerImpl implements AnnotationRpcController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final AnnotationDeepMapper annotationDeepMapper;

    private final AnnotationCustomMapper annotationCustomMapper;

    private final AnnotationService annotationService;

    @PostMapping
    public ResponseEntity<AnnotationDeepDto> post(@RequestBody final AnnotationCustomDto annotationCustomDto) {
        return Try
                .of(() -> {
                    final Annotation annotation = annotationCustomMapper.mapFrom(annotationCustomDto);
                    annotation.setId(null);
                    return annotation;
                })
                .flatMap(annotationService::annotate)
                .map(annotationDeepMapper::mapTo)
                .map(createdDto -> ResponseEntity
                        .created(linkTo(getClass()).slash(createdDto.getId()).toUri())
                        .body(createdDto))
                .onFailure(throwable -> logger.error("Error occurred while processing POST request", throwable))
                .getOrElse(() -> ResponseEntity
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .build());
    }
}
