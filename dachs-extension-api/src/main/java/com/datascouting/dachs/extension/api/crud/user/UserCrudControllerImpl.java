package com.datascouting.dachs.extension.api.crud.user;

import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.dto.deep.UserDeepDto;
import com.datascouting.dachs.extension.mapper.deep.UserDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.UserShallowMapper;
import com.datascouting.dachs.extension.service.user.UserCrudService;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Achilleas Naoumidis
 */
@RequestMapping("/users")
@RestController
@RequiredArgsConstructor
public class UserCrudControllerImpl implements UserCrudController {

    private final UserDeepMapper userDeepMapper;

    private final UserShallowMapper userShallowMapper;

    private final UserCrudService userCrudService;

    @Override
    public UserDeepMapper getMapper() {
        return userDeepMapper;
    }

    @Override
    public UserShallowMapper getReferenceMapper() {
        return userShallowMapper;
    }

    @Override
    public UserCrudService getService() {
        return userCrudService;
    }

    @Override
    public ResponseEntity<Page<UserDeepDto>> getAll(@QuerydslPredicate(root = User.class) Predicate predicate, Pageable pageable) {
        return getAllDefaultImplementation(predicate, pageable);
    }
}
