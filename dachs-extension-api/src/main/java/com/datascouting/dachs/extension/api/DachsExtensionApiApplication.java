package com.datascouting.dachs.extension.api;

import com.datascouting.dachs.extension.mapper.DachsExtensionMapperConfiguration;
import com.datascouting.dachs.extension.service.DachsExtensionServiceConfiguration;
import org.intellift.sol.controller.SolControllerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import({
        DachsExtensionServiceConfiguration.class,
        DachsExtensionMapperConfiguration.class,
        SolControllerConfiguration.class
})
@SpringBootApplication
public class DachsExtensionApiApplication {

    public static void main(final String... args) {
        SpringApplication.run(DachsExtensionApiApplication.class, args);
    }
}
