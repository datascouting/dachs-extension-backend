package com.datascouting.dachs.extension.api.validator.utils;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class ValidationPredicates {

    public static Predicate<String> isEmailAddress() {
        Pattern emailPattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        return email -> emailPattern.matcher(email).matches();
    }
}
