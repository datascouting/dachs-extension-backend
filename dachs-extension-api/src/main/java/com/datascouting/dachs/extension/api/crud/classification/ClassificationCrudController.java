package com.datascouting.dachs.extension.api.crud.classification;

import com.datascouting.dachs.extension.api.crud.AbstractCrudController;
import com.datascouting.dachs.extension.domain.entity.Classification;
import com.datascouting.dachs.extension.dto.deep.ClassificationDeepDto;
import com.datascouting.dachs.extension.dto.shallow.ClassificationShallowDto;
import com.datascouting.dachs.extension.mapper.deep.ClassificationDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.ClassificationShallowMapper;
import com.datascouting.dachs.extension.service.classification.ClassificationCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface ClassificationCrudController extends AbstractCrudController<Classification, ClassificationDeepDto, ClassificationShallowDto> {

    @Override
    ClassificationDeepMapper getMapper();

    @Override
    ClassificationShallowMapper getReferenceMapper();

    @Override
    ClassificationCrudService getService();
}
