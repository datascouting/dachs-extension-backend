package com.datascouting.dachs.extension.api.auth;

import com.datascouting.dachs.extension.api.validator.auth.RegisterValidator;
import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.service.user.UserCrudService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.function.Function;

@Controller
@RequiredArgsConstructor
public class AuthControllerImpl implements AuthController {

    private final UserCrudService userCrudService;
    private final PasswordEncoder passwordEncoder;
    private final RegisterValidator userValidator;

    @InitBinder
    protected void initBinder(final WebDataBinder binder) {
        binder.setValidator(userValidator);
    }

    @Override
    @GetMapping("/login")
    public String login() {
        return "auth/login";
    }

    @Override
    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("user", new User());
        return "auth/register";
    }

    @Override
    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) return "auth/register";

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userCrudService.create(user)
                .getOrElseThrow((Function<Throwable, RuntimeException>) RuntimeException::new);

        return "redirect:register-success";
    }

    @Override
    @GetMapping("/register-success")
    public String registerSuccess() {
        return "auth/register-success";
    }
}
