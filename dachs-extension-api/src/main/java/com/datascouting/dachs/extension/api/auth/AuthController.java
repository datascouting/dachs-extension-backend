package com.datascouting.dachs.extension.api.auth;

import com.datascouting.dachs.extension.domain.entity.User;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

public interface AuthController {
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    String login();

    @GetMapping("/register")
    String register(Model model);

    @PostMapping("/register")
    String registerUser(@Valid @ModelAttribute User user, BindingResult bindingResult);

    @GetMapping("/register-success")
    String registerSuccess();
}
