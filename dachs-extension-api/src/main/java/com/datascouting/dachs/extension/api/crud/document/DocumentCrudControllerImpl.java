package com.datascouting.dachs.extension.api.crud.document;

import com.datascouting.dachs.extension.domain.entity.Document;
import com.datascouting.dachs.extension.dto.deep.DocumentDeepDto;
import com.datascouting.dachs.extension.mapper.deep.DocumentDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.DocumentShallowMapper;
import com.datascouting.dachs.extension.service.document.DocumentCrudService;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Achilleas Naoumidis
 */
@RequestMapping("/documents")
@RestController
@RequiredArgsConstructor
public class DocumentCrudControllerImpl implements DocumentCrudController {

    private final DocumentDeepMapper documentDeepMapper;

    private final DocumentShallowMapper documentShallowMapper;

    private final DocumentCrudService documentCrudService;

    @Override
    public DocumentDeepMapper getMapper() {
        return documentDeepMapper;
    }

    @Override
    public DocumentShallowMapper getReferenceMapper() {
        return documentShallowMapper;
    }

    @Override
    public DocumentCrudService getService() {
        return documentCrudService;
    }

    @Override
    public ResponseEntity<Page<DocumentDeepDto>> getAll(@QuerydslPredicate(root = Document.class) final Predicate predicate,
                                                        final Pageable pageable) {
        return getAllDefaultImplementation(predicate, pageable);
    }
}
