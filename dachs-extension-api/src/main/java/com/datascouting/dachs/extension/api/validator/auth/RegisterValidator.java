package com.datascouting.dachs.extension.api.validator.auth;

import com.datascouting.dachs.extension.api.validator.AbstractValidator;
import com.datascouting.dachs.extension.domain.entity.QUser;
import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.service.user.UserCrudService;
import javaslang.collection.Stream;
import javaslang.collection.Traversable;
import javaslang.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;

import java.util.Objects;

import static com.datascouting.dachs.extension.api.validator.utils.ValidationHelpers.mustBeNull;
import static com.datascouting.dachs.extension.api.validator.utils.ValidationHelpers.notNullAnd;
import static com.datascouting.dachs.extension.api.validator.utils.ValidationPredicates.isEmailAddress;
import static org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace;

@Component
@RequiredArgsConstructor
public class RegisterValidator extends AbstractValidator {

    private final UserCrudService userCrudService;

    @Override
    public Class<User> getTargetClass() {
        return User.class;
    }

    @Override
    public void test(Object target, Errors errors) {
        rejectIfEmptyOrWhitespace(errors, "password", "auth.error.password.required");
        notNullAnd(errors, "password", "auth.error.password.greaterThanSixChars", (String password) -> password.length() > 6);

        rejectIfEmptyOrWhitespace(errors, "email", "auth.error.email.required");
        notNullAnd(errors, "email", "auth.error.email.notValid", isEmailAddress());

        String value = String.valueOf(errors.getFieldValue("email"));
        if (value != null && StringUtils.hasText(value)) {
            userCrudService.findAll(QUser.user.email.eq(value))
                    .toOption()
                    .flatMap(Traversable::headOption)
                    .forEach(user -> errors.rejectValue("email", "auth.error.email.mustByUnique"));
        }

        mustBeNull(errors, "roles", "auth.error.roles.mustBeNull");
    }
}
