package com.datascouting.dachs.extension.api.crud.source;

import com.datascouting.dachs.extension.api.crud.AbstractCrudController;
import com.datascouting.dachs.extension.domain.entity.Source;
import com.datascouting.dachs.extension.dto.deep.SourceDeepDto;
import com.datascouting.dachs.extension.dto.shallow.SourceShallowDto;
import com.datascouting.dachs.extension.mapper.deep.SourceDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.SourceShallowMapper;
import com.datascouting.dachs.extension.service.source.SourceCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface SourceCrudController extends AbstractCrudController<Source, SourceDeepDto, SourceShallowDto> {

    @Override
    SourceDeepMapper getMapper();

    @Override
    SourceShallowMapper getReferenceMapper();

    @Override
    SourceCrudService getService();
}
