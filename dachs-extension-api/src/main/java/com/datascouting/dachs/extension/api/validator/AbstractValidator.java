package com.datascouting.dachs.extension.api.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static java.util.Objects.requireNonNull;

abstract public class AbstractValidator implements Validator {

    @Override
    final public boolean supports(Class<?> clazz) {
        return getTargetClass().equals(clazz);
    }

    @Override
    final public void validate(Object target, Errors errors) {
        requireNonNull(target, getTargetClass().getSimpleName() + " object is null.");

        test(target, errors);
    }

    abstract public Class<?> getTargetClass();

    /**
     * Use this method instead of validate
     *
     * @param target
     * @param errors
     */
    abstract public void test(Object target, Errors errors);
}
