package com.datascouting.dachs.extension.api.crud.role;

import com.datascouting.dachs.extension.api.crud.AbstractCrudController;
import com.datascouting.dachs.extension.domain.entity.Role;
import com.datascouting.dachs.extension.dto.deep.RoleDeepDto;
import com.datascouting.dachs.extension.dto.shallow.RoleShallowDto;
import com.datascouting.dachs.extension.mapper.deep.RoleDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.RoleShallowMapper;
import com.datascouting.dachs.extension.service.role.RoleCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface RoleCrudController extends AbstractCrudController<Role, RoleDeepDto, RoleShallowDto> {

    @Override
    RoleDeepMapper getMapper();

    @Override
    RoleShallowMapper getReferenceMapper();

    @Override
    RoleCrudService getService();
}
