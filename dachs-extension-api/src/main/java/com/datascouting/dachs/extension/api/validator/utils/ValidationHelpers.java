package com.datascouting.dachs.extension.api.validator.utils;

import org.springframework.validation.Errors;

import java.util.Collection;
import java.util.Objects;
import java.util.function.Predicate;

public class ValidationHelpers {

    public static void throwIfNull(final Errors errors, final String field) {
        final Object object = errors.getFieldValue(field);

        if (Objects.isNull(object)) {
            throw new IllegalArgumentException(field + " is null.");
        }
    }

    public static void requiredObject(final Errors errors, final String field) {
        final Object object = errors.getFieldValue(field);

        if (Objects.isNull(object)) {
            errors.rejectValue(field, field + ".required");
        }
    }

    public static void notEmptyCollection(final Errors errors, final String field) {
        final Object object = errors.getFieldValue(field);

        throwIfNull(errors, field);

        if (((Collection) object).isEmpty()) {
            errors.rejectValue(field, field + ".empty");
        }
    }

    public static <T> void notNullAnd(final Errors errors, final String field, final String errorCode, final Predicate<T> predicate) {
        final Object rawObject = errors.getFieldValue(field);

        if (Objects.isNull(rawObject)) {
            errors.rejectValue(field, field + ".required");
        } else {
            @SuppressWarnings("unchecked") final T object = (T) rawObject;

            if (predicate.negate().test(object)) {
                errors.rejectValue(field, errorCode);
            }
        }
    }

    public static <T> void nullOr(final Errors errors, final String field, final String errorCode, final Predicate<T> predicate) {
        final Object rawObject = errors.getFieldValue(field);

        if (Objects.nonNull(rawObject)) {
            @SuppressWarnings("unchecked") final T object = (T) rawObject;

            if (predicate.test(object)) {
                errors.rejectValue(field, errorCode);
            }
        }
    }

    public static void mustBeNull(final Errors errors, final String field, final String errorCode) {
        final Object rawObject = errors.getFieldValue(field);

        if (Objects.nonNull(rawObject)) {
            errors.rejectValue(field, errorCode);
        }
    }
}
