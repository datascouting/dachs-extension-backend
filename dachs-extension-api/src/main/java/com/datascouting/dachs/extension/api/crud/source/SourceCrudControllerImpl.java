package com.datascouting.dachs.extension.api.crud.source;

import com.datascouting.dachs.extension.domain.entity.Source;
import com.datascouting.dachs.extension.dto.deep.SourceDeepDto;
import com.datascouting.dachs.extension.mapper.deep.SourceDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.SourceShallowMapper;
import com.datascouting.dachs.extension.service.source.SourceCrudService;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Achilleas Naoumidis
 */
@RequestMapping("/sources")
@RestController
@RequiredArgsConstructor
public class SourceCrudControllerImpl implements SourceCrudController {

    private final SourceDeepMapper sourceDeepMapper;

    private final SourceShallowMapper sourceShallowMapper;

    private final SourceCrudService sourceCrudService;

    @Override
    public SourceDeepMapper getMapper() {
        return sourceDeepMapper;
    }

    @Override
    public SourceShallowMapper getReferenceMapper() {
        return sourceShallowMapper;
    }

    @Override
    public SourceCrudService getService() {
        return sourceCrudService;
    }

    @Override
    public ResponseEntity<Page<SourceDeepDto>> getAll(@QuerydslPredicate(root = Source.class) Predicate predicate, Pageable pageable) {
        return getAllDefaultImplementation(predicate, pageable);
    }
}
