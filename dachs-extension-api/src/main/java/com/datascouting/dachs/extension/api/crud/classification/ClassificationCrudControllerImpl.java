package com.datascouting.dachs.extension.api.crud.classification;

import com.datascouting.dachs.extension.domain.entity.Classification;
import com.datascouting.dachs.extension.dto.deep.ClassificationDeepDto;
import com.datascouting.dachs.extension.mapper.deep.ClassificationDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.ClassificationShallowMapper;
import com.datascouting.dachs.extension.service.classification.ClassificationCrudService;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Achilleas Naoumidis
 */
@RequestMapping("/classifications")
@RestController
@RequiredArgsConstructor
public class ClassificationCrudControllerImpl implements ClassificationCrudController {

    private final ClassificationDeepMapper classificationDeepMapper;

    private final ClassificationShallowMapper classificationShallowMapper;

    private final ClassificationCrudService classificationCrudService;

    @Override
    public ClassificationDeepMapper getMapper() {
        return classificationDeepMapper;
    }

    @Override
    public ClassificationShallowMapper getReferenceMapper() {
        return classificationShallowMapper;
    }

    @Override
    public ClassificationCrudService getService() {
        return classificationCrudService;
    }

    @Override
    public ResponseEntity<Page<ClassificationDeepDto>> getAll(@QuerydslPredicate(root = Classification.class) final Predicate predicate,
                                                              final Pageable pageable) {
        return getAllDefaultImplementation(predicate, pageable);
    }
}
