package com.datascouting.dachs.extension.api.crud.user;

import com.datascouting.dachs.extension.api.crud.AbstractCrudController;
import com.datascouting.dachs.extension.domain.entity.User;
import com.datascouting.dachs.extension.dto.deep.UserDeepDto;
import com.datascouting.dachs.extension.dto.shallow.UserShallowDto;
import com.datascouting.dachs.extension.mapper.deep.UserDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.UserShallowMapper;
import com.datascouting.dachs.extension.service.user.UserCrudService;

/**
 * @author Achilleas Naoumidis
 */
public interface UserCrudController extends AbstractCrudController<User, UserDeepDto, UserShallowDto> {

    @Override
    UserDeepMapper getMapper();

    @Override
    UserShallowMapper getReferenceMapper();

    @Override
    UserCrudService getService();
}
