package com.datascouting.dachs.extension.api.crud.annotation;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import com.datascouting.dachs.extension.dto.deep.AnnotationDeepDto;
import com.datascouting.dachs.extension.mapper.deep.AnnotationDeepMapper;
import com.datascouting.dachs.extension.mapper.shallow.AnnotationShallowMapper;
import com.datascouting.dachs.extension.service.annotation.AnnotationCrudService;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Achilleas Naoumidis
 */
@RequestMapping("/annotations")
@RestController
@RequiredArgsConstructor
public class AnnotationCrudControllerImpl implements AnnotationCrudController {

    private final AnnotationDeepMapper annotationDeepMapper;

    private final AnnotationShallowMapper annotationShallowMapper;

    private final AnnotationCrudService annotationCrudService;

    @Override
    public AnnotationDeepMapper getMapper() {
        return annotationDeepMapper;
    }

    @Override
    public AnnotationShallowMapper getReferenceMapper() {
        return annotationShallowMapper;
    }

    @Override
    public AnnotationCrudService getService() {
        return annotationCrudService;
    }

    @Override
    public ResponseEntity<Page<AnnotationDeepDto>> getAll(@QuerydslPredicate(root = Annotation.class) final Predicate predicate,
                                                          final Pageable pageable) {
        return getAllDefaultImplementation(predicate, pageable);
    }
}
