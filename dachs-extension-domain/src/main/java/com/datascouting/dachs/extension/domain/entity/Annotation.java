package com.datascouting.dachs.extension.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

import static javax.persistence.FetchType.EAGER;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "annotation")
@Entity
public class Annotation extends AbstractEntity {

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = "document_id", nullable = false)
    private Document document;

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = "classification_id", nullable = false)
    private Classification classification;

    @Column(name = "notes")
    private String notes;
}
