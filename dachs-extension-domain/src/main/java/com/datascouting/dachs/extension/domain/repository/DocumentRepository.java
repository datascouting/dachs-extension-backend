package com.datascouting.dachs.extension.domain.repository;

import com.datascouting.dachs.extension.domain.entity.Document;
import javaslang.control.Option;
import org.springframework.stereotype.Repository;

/**
 * @author Achilleas Naoumidis
 */
@Repository
public interface DocumentRepository extends AbstractRepository<Document> {

    Option<Document> findOneByReferenceId(String referenceId);
}
