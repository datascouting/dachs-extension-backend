package com.datascouting.dachs.extension.domain.repository;

import com.datascouting.dachs.extension.domain.entity.Annotation;
import org.springframework.stereotype.Repository;

/**
 * @author Achilleas Naoumidis
 */
@Repository
public interface AnnotationRepository extends AbstractRepository<Annotation> {
}
