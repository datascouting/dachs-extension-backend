package com.datascouting.dachs.extension.domain.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.intellift.sol.domain.Identifiable;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Achilleas Naoumidis
 */
@Data
@MappedSuperclass
public abstract class AbstractEntity implements Identifiable<String> {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "com.datascouting.dachs.extension.domain.generator.UUIDGenerator")
    protected String id;

    @Column(name = "created_at")
    protected Date createdAt;

    @Column(name = "updated_at")
    protected Date updatedAt;

    @PrePersist
    protected void onPrePersist() {
        createdAt = updatedAt = new Date();
    }

    @PreUpdate
    protected void onPreUpdate() {
        updatedAt = new Date();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "id='" + id + '\'' +
                '}';
    }
}
