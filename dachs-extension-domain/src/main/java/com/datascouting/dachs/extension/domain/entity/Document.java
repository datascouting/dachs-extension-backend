package com.datascouting.dachs.extension.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

import static javax.persistence.FetchType.EAGER;

/**
 * @author Achilleas Naoumidis
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Table(name = "document")
@Entity
public class Document extends AbstractEntity {

    @ManyToOne(fetch = EAGER)
    @JoinColumn(name = "source_id", nullable = false)
    private Source source;

    @Column(name = "reference_id", nullable = false, unique = true)
    private String referenceId;

    @Column(columnDefinition = "TEXT", name = "text", nullable = false)
    private String text;
}
