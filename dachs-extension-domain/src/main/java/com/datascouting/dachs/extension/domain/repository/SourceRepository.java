package com.datascouting.dachs.extension.domain.repository;

import com.datascouting.dachs.extension.domain.entity.Source;
import org.springframework.stereotype.Repository;

/**
 * @author Achilleas Naoumidis
 */
@Repository
public interface SourceRepository extends AbstractRepository<Source> {
}
