package com.datascouting.dachs.extension.domain.repository;

import com.datascouting.dachs.extension.domain.entity.AbstractEntity;
import org.intellift.sol.domain.querydsl.jpa.repository.QueryDslJpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author Achilleas Naoumidis
 */
@NoRepositoryBean
public interface AbstractRepository<E extends AbstractEntity> extends QueryDslJpaRepository<E, String> {
}
