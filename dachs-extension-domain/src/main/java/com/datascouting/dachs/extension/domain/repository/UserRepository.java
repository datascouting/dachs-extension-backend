package com.datascouting.dachs.extension.domain.repository;

import com.datascouting.dachs.extension.domain.entity.User;
import javaslang.control.Option;
import org.springframework.stereotype.Repository;

/**
 * @author Achilleas Naoumidis
 */
@Repository
public interface UserRepository extends AbstractRepository<User> {

    Option<User> findByEmail(String email);
}
