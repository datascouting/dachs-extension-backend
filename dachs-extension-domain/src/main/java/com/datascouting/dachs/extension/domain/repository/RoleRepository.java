package com.datascouting.dachs.extension.domain.repository;

import com.datascouting.dachs.extension.domain.entity.Role;
import org.springframework.stereotype.Repository;

/**
 * @author Achilleas Naoumidis
 */
@Repository
public interface RoleRepository extends AbstractRepository<Role> {
}
