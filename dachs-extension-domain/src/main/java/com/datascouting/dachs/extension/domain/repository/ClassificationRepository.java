package com.datascouting.dachs.extension.domain.repository;

import com.datascouting.dachs.extension.domain.entity.Classification;
import org.springframework.stereotype.Repository;

/**
 * @author Achilleas Naoumidis
 */
@Repository
public interface ClassificationRepository extends AbstractRepository<Classification> {
}
