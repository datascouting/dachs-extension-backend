create table if not exists classification
(
  id          varchar(255) not null constraint classification_pk primary key,
  description varchar(255),
  name        varchar(255) not null
);

create table if not exists source
(
  id          varchar(255) not null constraint source_pk primary key,
  description varchar(255),
  domain      varchar(255) not null constraint source_domain_unique unique,
  name        varchar(255) not null
);

create table if not exists role
(
  id          varchar(255) not null constraint role_pk primary key,
  description varchar(255),
  name        varchar(255) not null
);

create table if not exists "user"
(
  id       varchar(255) not null constraint user_pk primary key,
  email    varchar(255) not null unique,
  password varchar(255) not null
);

create table if not exists user_role
(
  user_id varchar(255) not null constraint user_role_user_fk references "user",
  role_id varchar(255) not null constraint user_role_role_fk references role,
  constraint user_role_pk primary key (user_id, role_id)
);

create table if not exists document
(
  id           varchar(255) not null constraint document_pk primary key,
  reference_id varchar(255) not null constraint document_reference_unique unique,
  text         text         not null,
  source_id    varchar(255) not null constraint document_source_fk references source
);

create table annotation
(
  id                varchar(255) not null constraint annotation_pk primary key,
  notes             varchar(255),
  classification_id varchar(255) not null constraint annotation_classification_fk references classification,
  document_id       varchar(255) not null constraint annotation_document_fk references document
);
