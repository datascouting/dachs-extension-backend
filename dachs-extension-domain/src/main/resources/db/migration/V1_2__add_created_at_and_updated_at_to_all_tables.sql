alter table annotation
    add column created_at timestamp not null default now(),
    add column updated_at timestamp not null default now();

alter table classification
    add column created_at timestamp not null default now(),
    add column updated_at timestamp not null default now();

alter table document
    add column created_at timestamp not null default now(),
    add column updated_at timestamp not null default now();

alter table role
    add column created_at timestamp not null default now(),
    add column updated_at timestamp not null default now();

alter table source
    add column created_at timestamp not null default now(),
    add column updated_at timestamp not null default now();

alter table "user"
    add column created_at timestamp not null default now(),
    add column updated_at timestamp not null default now();

alter table user_role
    add column created_at timestamp not null default now(),
    add column updated_at timestamp not null default now();
