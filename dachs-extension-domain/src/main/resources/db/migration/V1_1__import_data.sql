INSERT INTO public.classification (id, description, name) VALUES ('cce20460-bafd-4a29-a146-b9cfb4cfadb3', null, 'hate-speech');
INSERT INTO public.classification (id, description, name) VALUES ('c38eb33d-027b-4ed3-b458-51534a1dd4ec', null, 'personal-attack');

INSERT INTO public.source (id, description, domain, name) VALUES ('f2879c71-ba51-461a-9c9a-845106e1e233', null, 'https://twitter.com', 'Twitter');
INSERT INTO public.source (id, description, domain, name) VALUES ('03f45a99-10b8-4cfd-bf5a-13c2413b7e32', null, 'https://youtube.com', 'YouTube');

INSERT INTO public.role (id, description, name) VALUES ('1372a96a-4199-442d-9a20-ca909302f1cc', null, 'ROLE_ADMIN');
INSERT INTO public.role (id, description, name) VALUES ('a812d165-eec5-447f-a6c9-b17470f301b0', null, 'ROLE_CURATOR');

INSERT INTO public."user" (id, email, password) VALUES ('4c9fc929-0753-4177-8eca-5dcdc17c8a3b', 'anaumidis@datascouting.com', '$2a$10$P77ii1fGo3IQLBLMqASu7OhpIarRTkA/9xyvk7it8ySvPcfL6a.uy');
INSERT INTO public."user" (id, email, password) VALUES ('8ffab36b-2780-4829-8a3b-48cdb533ac99', 'doro@datascouting.com', '$2a$10$wNAenZ5jyICGPwUBXoKNWuSC0zJHhwWfw0qA8hmW9wvJ1GLrklK7C');

INSERT INTO public.user_role (user_id, role_id) VALUES ('4c9fc929-0753-4177-8eca-5dcdc17c8a3b', '1372a96a-4199-442d-9a20-ca909302f1cc');
INSERT INTO public.user_role (user_id, role_id) VALUES ('8ffab36b-2780-4829-8a3b-48cdb533ac99', 'a812d165-eec5-447f-a6c9-b17470f301b0');

